public class Pesel {

    private String pesel;

    Pesel(String pesel) throws Exception {
        if(pesel.length() != 11) {
            System.err.println("Wrong pesel length!");
            throw new Exception();
        } else {
            this.pesel = pesel;
        }
    }

    public String checkSex() throws Exception {
        this.checkCorrectness();
        char[] peselCharArr = this.pesel.toCharArray();
        char sexChar = peselCharArr[9];
        int sexInt = Character.getNumericValue(sexChar);
        if(sexInt % 2 == 0) {
            return "Female";
        } else {
            return "Male";
        }
    }

    public boolean checkCorrectness() throws Exception {
        char[] peselCharArr = this.pesel.toCharArray();
        int peselIntArr[] = new int[11];
        for(int i=0; i<11; i++) {
           peselIntArr[i] = Character.valueOf(peselCharArr[i]);
        }

        int checkSum = 1 * peselIntArr[0] + 3 * peselIntArr[1] + 7 * peselIntArr[2] + 9 * peselIntArr[3] +
                1 * peselIntArr[4] + 3 * peselIntArr[5] + 7 * peselIntArr[6] + 9 * peselIntArr[7] + 1 * peselIntArr[8] +
                3 * peselIntArr[9] + 1 * peselIntArr[10];

        if(checkSum % 10 != 0) {
             System.err.println("Wrong pesel number!");
             throw new Exception();
        }


        System.out.println("Correct pesel number!");
        return true;
    }
}
